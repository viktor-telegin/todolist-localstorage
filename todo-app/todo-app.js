(function () {
  let storageList = [];
  let testTitle = '';

  function createAppTitle (title) {
    let appTitle = document.createElement('h2');
    appTitle.innerHTML = title;
    return appTitle;
  };

  function createTodoItemForm () {
    let form = document.createElement('form');
    let input = document.createElement('input');
    let buttonWrapper = document.createElement('div');
    let button = document.createElement('button');

    form.classList.add('input-group', 'mb-3');
    input.classList.add('form-control');
    input.placeholder = 'Добавьте новое дело';
    buttonWrapper.classList.add('input-group-append');
    button.classList.add('btn', 'btn-primary');
    button.textContent = 'Добавить';
    button.disabled = true

    buttonWrapper.append(button);
    form.append(input);
    form.append(buttonWrapper);

    input.addEventListener('input', function() { button.disabled = input.value == ''});
    return {
      form, input, button
    };
  };

  function createTodoList () {
    let list = document.createElement('ul');
    list.classList.add('list-group');
    return list;
  };

  function createTodoItem (name, done) {
    let item = document.createElement('li');
    let buttonGroup = document.createElement('div');
    let doneButton = document.createElement('button');
    let deleteButton = document.createElement('button');

    item.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
    item.textContent = name;

    buttonGroup.classList.add('btn-group', 'btn-group-sm');
    doneButton.classList.add('btn', 'btn-success');
    doneButton.textContent = 'Справились';
    deleteButton.classList.add('btn', 'btn-danger');
    deleteButton.textContent = 'Передумал';
    if (done == true) {item.classList.toggle('list-group-item-success')}
    else {item.classList.remove('list-group-item-success')}

    buttonGroup.append(doneButton);
    buttonGroup.append(deleteButton);
    item.append(buttonGroup);

    doneButton.addEventListener('click', function () {
      item.classList.toggle('list-group-item-success');
      let findTask = storageList.findIndex(task => task.name === name);
      storageList[findTask].done = !storageList[findTask].done;
      localStorage.setItem(testTitle, JSON.stringify(storageList));
    });
    deleteButton.addEventListener('click', function () {
      if (confirm('Точно?')) {
        item.remove();
        let findTask = storageList.findIndex(task => task.name === name);
        storageList.splice(findTask,1);
        localStorage.setItem(testTitle, JSON.stringify(storageList));
      }
    });

    return {
      item, doneButton, deleteButton
    }

  }

  function createTodoApp (container, title = 'Мой список', defaultItems =[]) {

    testTitle = title;
    let todoAppTitle = createAppTitle(title);
    let todoItemForm = createTodoItemForm();
    let todoList = createTodoList();


    container.append(todoAppTitle);
    container.append(todoItemForm.form);
    container.append(todoList);

    for (let item of defaultItems) {
      let itemEl = createTodoItem(item.name, item.done)
      todoList.append(itemEl.item);

    };

    todoItemForm.form.addEventListener('submit', function(e) {
      e.preventDefault();

      if (!todoItemForm.input.value) {
        return
      }

      let todoItem = createTodoItem(todoItemForm.input.value);

      storageList.push({name: todoItemForm.input.value, done: false});




      todoList.append(todoItem.item);


      todoItemForm.input.value = '';

      localStorage.setItem(title, JSON.stringify(storageList))
    })

    if (window.performance) {
      let restoredList = JSON.parse(localStorage.getItem(title));
      storageList = [...storageList, ...restoredList]
      for (let item of storageList) {
        let itemRestored = createTodoItem(item.name, item.done)
        todoList.append(itemRestored.item)
      }
    }
  }
  window.createTodoApp = createTodoApp;
})
();



